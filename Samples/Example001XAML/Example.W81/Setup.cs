using MvvmCross.Platform;
using MvvmCross.Platform.Platform;
using MvvmCross.WindowsCommon.Platform;
using MvvmCross.Forms.Presenter.Core;
using MvvmCross.Forms.Presenter.Windows81;
using MvvmCross.Core.ViewModels;
using MvvmCross.Core.Views;
using Xamarin.Forms;
using MvvmCross.WindowsCommon.Views;
using XamlControls = Windows.UI.Xaml.Controls;
using Windows.ApplicationModel.Activation;
using System.Collections.Generic;
using System.Reflection;
using System;

namespace Example.W81
{
    public class Setup : MvxWindowsSetup
    {
        private readonly LaunchActivatedEventArgs _launchActivatedEventArgs;

        public Setup(XamlControls.Frame rootFrame, LaunchActivatedEventArgs e) : base(rootFrame)
        {
            _launchActivatedEventArgs = e;
        }

        protected override IMvxApplication CreateApp()
        {
            return new Example.App();
        }

        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugTrace();
        }

        protected override IMvxWindowsViewPresenter CreateViewPresenter(IMvxWindowsFrame rootFrame)
        {
            Forms.Init(_launchActivatedEventArgs);

            var xamarinFormsApp = new MvxFormsApp();
            var presenter = new MvxFormsWindows81PagePresenter(rootFrame, xamarinFormsApp);
            Mvx.RegisterSingleton<IMvxViewPresenter>(presenter);

            return presenter;
        }

        //protected override IEnumerable<Assembly> GetViewAssemblies()
        //{
        //    var list = new List<Assembly>();
        //    list.AddRange(base.GetViewAssemblies());
        //    list.Add(typeof(Example.Pages.FirstPage).GetTypeInfo().Assembly);

        //    return list;
        //}

        //protected override IEnumerable<Assembly> GetViewModelAssemblies()
        //{
        //    var list = new List<Assembly>();
        //    list.AddRange(base.GetViewModelAssemblies());
        //    list.Add(typeof(Example.ViewModels.FirstViewModel).GetTypeInfo().Assembly);

        //    return list;
        //}

        protected override void InitializeViewLookup()
        {
            var viewModelLookup = new Dictionary<Type, Type>
            {
                {typeof(ViewModels.AboutViewModel), typeof(Pages.AboutPage)},
                {typeof(ViewModels.FirstViewModel), typeof(Pages.FirstPage)}
            };

            var container = Mvx.Resolve<IMvxViewsContainer>();
            container.AddAll(viewModelLookup);
        }
    }
}